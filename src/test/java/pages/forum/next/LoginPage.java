package pages.forum.next;

import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;

import com.cucumber.listener.Reporter;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import lib.selenium.WebDriverServiceImpl;

public class LoginPage extends WebDriverServiceImpl {

	String url;

	@Given("Launch the browser and Load the DP Url")
	public void launch_DP_Url() throws Throwable {
		loadUrl("dp_url");
		url = "dp";
	}

	@Given("Launch the browser and Load the CP Url")
	public void launch_CP_Url() throws Throwable {
		loadUrl("cp_url");
		url = "cp";
	}	


	@And("^Enter Credentials and login(.*)$")
	public void enter_valid_Username_and_password(int arg1) throws Throwable {

		String userName = readData(0, arg1, "ForumLogin");
		type(locateElement("id", "username_id"), userName);

		String pwd = readData(1, arg1, "ForumLogin");
		type(locateElement("id", "password_id"), pwd);

		Reporter.addStepLog("Login Details entered in respective field");
		Reporter.addScreenCaptureFromPath(captureScreenShot("LoginDetailsEntered"));


		if(url.equals("dp")){
			click(locateElement("xpath", "dp_submit_xp"));
		}
		else if(url.equals("cp")){
			click(locateElement("xpath", "cp_submit_xp"));
		}

		try {
			driver.findElementByXPath(prop.getProperty("signout_xp"));
			Reporter.addStepLog("Home Page Launched");
			Reporter.addScreenCaptureFromPath(captureScreenShot("HomePage"));

		} catch (NoSuchElementException e) {			
			Reporter.addStepLog("Home Page is NOT Launched");
			Reporter.addScreenCaptureFromPath(captureScreenShot("LoginError"));
			Assert.assertEquals(false, true, "FAILED");

		}



	}




}
