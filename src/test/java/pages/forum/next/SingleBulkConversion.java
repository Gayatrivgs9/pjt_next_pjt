package pages.forum.next;

import java.io.IOException;

import org.openqa.selenium.WebElement;

import cucumber.api.java.en.And;
import lib.selenium.WebDriverServiceImpl;

public class SingleBulkConversion extends WebDriverServiceImpl{

	@And("Click on populate button")
	public void clickPopulateButton() throws IOException {
		click(locateElement("name", "populate_name"));
	}  

	@And("Select the required Sales Order")
	public void clickRequiredSalsOrder() throws IOException {
		WebElement element = locateElement("name", "requiredSales_name");
		if(element.isSelected() != true) {
			click(element);
		}
	}
	@And("Click on Single click bulk conversion")
	public void clickAutoSubmitOrderId() throws IOException {
		WebElement element = locateElement("id", "auto_submit_order_id");
		if(element.isSelected() != true) {
			click(element); 
		}
	}
	@And("Click on Credit Bills")
	public void clickCreditBills() throws IOException, InterruptedException {
		click(locateElement("name", "creditBills_name"));
		Thread.sleep(3000);
		acceptAlert();
	}
}
























