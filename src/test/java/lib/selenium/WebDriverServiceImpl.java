package lib.selenium;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.cucumber.listener.Reporter;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import pages.forum.next.Hooks;

public class WebDriverServiceImpl {
	public static RemoteWebDriver driver;
	static WebDriverWait wait;
	public static Properties prop;
	static RemoteWebDriver screenshotdriver;
	static String path = System.getProperty("user.dir");
	static ExtentReports report;
	static ExtentTest testlog;
	public String environment = "local";
	//public String environment = "AWS";

	public WebDriverServiceImpl() {
		driver = Hooks.driver;
		prop = new Properties();
		try {
			if(environment.equals("local")) {
				prop.load(new FileInputStream(new File("./src/test/java/propertiesFile/locators.properties")));
			} else {
				prop.load(new FileInputStream(new File("classes//propertiesFile//locators.properties")));	
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void startApp(String browser, String url) throws IOException {
		try {
			if(environment.equals("local")) {
				if(browser.equalsIgnoreCase("chrome")){
					System.setProperty("webdriver.chrome.driver", "./src/test/java/drivers/chromedriver.exe");
					driver = new ChromeDriver(); 
				}else {
					System.setProperty("webdriver.gecko.driver", "./src/test/java/drivers/geckodriver.exe");
					driver = new FirefoxDriver();
				}
				driver.manage().window().maximize();
				driver.get(prop.getProperty(url));
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				Reporter.addStepLog("The browser: "+browser+" launched successfully");
				Reporter.addScreenCaptureFromPath(captureScreenShot("BrowserLaunched")); 
			} else {
				if(browser.equalsIgnoreCase("chrome")){
					System.setProperty("webdriver.chrome.driver", "classes//drivers//chromedriver.exe");
					driver = new ChromeDriver(); 
				}else {
					System.setProperty("webdriver.gecko.driver", "classes//drivers//geckodriver.exe");
					driver = new FirefoxDriver();
				}
				driver.manage().window().maximize();
				driver.get(prop.getProperty(url));
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				Reporter.addStepLog("The browser: "+browser+" launched successfully");
				Reporter.addScreenCaptureFromPath(captureScreenShot("BrowserLaunched"));
			}

		} catch (WebDriverException e) {			
			Reporter.addStepLog("The browser: "+browser+" could not be launched");
			Reporter.addScreenCaptureFromPath(captureScreenShot("BrowserNotLaunched"));
		}
	}
	public void startApp(String browser) throws IOException {
		try {
			if(environment.equals("local")) {
				if(browser.equalsIgnoreCase("chrome")){
					System.setProperty("webdriver.chrome.driver", "./src/test/java/drivers/chromedriver.exe");
					driver = new ChromeDriver();				
				}else if(browser.equalsIgnoreCase("firefox")) {
					System.setProperty("webdriver.gecko.driver", "./src/test/java/drivers/geckodriver.exe");
					driver = new FirefoxDriver();
				}
				driver.manage().window().maximize();
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				Reporter.addStepLog("The browser: "+browser+" launched successfully");
				Reporter.addScreenCaptureFromPath(captureScreenShot("BrowserLaunched"));
			}else {
				FirefoxBinary firefoxBinary = new FirefoxBinary();
				firefoxBinary.addCommandLineOptions("--headless");
				System.setProperty("webdriver.gecko.driver", "/usr/local/bin/geckodriver");
				FirefoxOptions firefoxOptions = new FirefoxOptions();             
				firefoxOptions.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, false);             
				firefoxOptions.setBinary(firefoxBinary);
				driver = new FirefoxDriver(firefoxOptions);          
				driver.manage().window().maximize();
				driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				System.out.println("Test Started");
				//Reporter.addStepLog("The browser: "+browser+" launched successfully");
			}} catch (WebDriverException e) {			
				Reporter.addStepLog("The browser: "+browser+" could not be launched");
				Reporter.addScreenCaptureFromPath(captureScreenShot("BrowserNotLaunched"));
			}
	}

	public void loadUrl(String url) throws IOException {
		try {
			driver.get(prop.getProperty(url)); 
			Reporter.addStepLog("The Url: "+prop.getProperty(url)+" Launched Successfully");
			Reporter.addScreenCaptureFromPath(captureScreenShot("UrlLaunched"));
		} catch (WebDriverException e) {
			Reporter.addStepLog("The Url: "+prop.getProperty(url)+" Not Launched");
			Reporter.addScreenCaptureFromPath(captureScreenShot("UrlNotLaunched"));
		}
	}
	public WebElement locateElement(String locator, String locValue) throws IOException {

		try {
			switch (locator) {
			case "id": return driver.findElementById(prop.getProperty(locValue));
			case "name": return driver.findElementByName(prop.getProperty(locValue));
			case "class": return driver.findElementByClassName(prop.getProperty(locValue));
			case "link" : return driver.findElementByLinkText(prop.getProperty(locValue));
			case "xpath": return driver.findElementByXPath(prop.getProperty(locValue));
			case "tagname": return driver.findElementByTagName(prop.getProperty(locValue));
			default:
				break;
			}
		} catch (NoSuchElementException e) {
			Reporter.addStepLog("The element with locator "+locator+" not found.");
			Reporter.addScreenCaptureFromPath(captureScreenShot("ElementFound"));
		} catch (WebDriverException e) {
			Reporter.addStepLog("Unknown exception occured while finding "+locator+" with the value "+locValue);
			Reporter.addScreenCaptureFromPath(captureScreenShot("ElmentNotFound"));
		}
		return null;
	}
	public List<WebElement> locateElements(String locator, String locValue) throws IOException {

		try {
			switch (locator) {
			case "id": return driver.findElementsById(prop.getProperty(locValue));
			case "name": return driver.findElementsByName(prop.getProperty(locValue));
			case "class": return driver.findElementsByClassName(prop.getProperty(locValue));
			case "link" : return driver.findElementsByLinkText(prop.getProperty(locValue));
			case "xpath": return driver.findElementsByXPath(prop.getProperty(locValue));
			case "tagname": return driver.findElementsByTagName(prop.getProperty(locValue));
			default:
				break;
			}
		} catch (NoSuchElementException e) {
			Reporter.addStepLog("The element with locator "+locator+" not found.");
			Reporter.addScreenCaptureFromPath(captureScreenShot("ElementFound"));
		} catch (WebDriverException e) {
			Reporter.addStepLog("Unknown exception occured while finding "+locator+" with the value "+locValue);
			Reporter.addScreenCaptureFromPath(captureScreenShot("ElementNotFound"));
		}
		return null;
	}
	public void type(WebElement ele, String data) throws IOException {
		try {
			ele.clear();
			ele.sendKeys(data);
			Reporter.addStepLog("The data: "+data+" entered successfully in the field :"+ele);
			Reporter.addScreenCaptureFromPath(captureScreenShot("DataEntered"));
		} catch (InvalidElementStateException e) {
			Reporter.addStepLog("The data: "+data+" could not be entered in the field :"+ele);
			Reporter.addScreenCaptureFromPath(captureScreenShot("DataNotEnterd"));
		} catch (WebDriverException e) {
			Reporter.addStepLog("Unknown exception occured while entering "+data+" in the field :"+ele); 
			Reporter.addScreenCaptureFromPath(captureScreenShot("DataNotEnterd"));
		}
	}
	public void typeWithKeyPress(WebElement ele, String data) throws IOException {
		try {
			ele.clear();
			ele.sendKeys(data, Keys.ENTER);
			Reporter.addStepLog("The data: "+data+" entered successfully in the field :"+ele);
			Reporter.addScreenCaptureFromPath(captureScreenShot("DataEntered"));
		} catch (InvalidElementStateException e) {
			Reporter.addStepLog("The data: "+data+" could not be entered in the field :"+ele);
			Reporter.addScreenCaptureFromPath(captureScreenShot("DataNotEnterd"));
		} catch (WebDriverException e) {
			Reporter.addStepLog("Unknown exception occured while entering "+data+" in the field :"+ele); 
			Reporter.addScreenCaptureFromPath(captureScreenShot("DataNotEnterd"));
		}
	}


	public void click(WebElement ele) throws IOException {
		String text = "";
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ele));
			text = ele.getText();
			ele.click();
			Reporter.addStepLog("Clicked on  the Element ::: " + text);
			Reporter.addScreenCaptureFromPath(captureScreenShot("ElementClicked"));
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
			System.out.println(e.getMessage());
			Reporter.addStepLog(e.getMessage());
			Reporter.addStepLog("Problem in clicking in xPath:::: " + text);
			Reporter.addScreenCaptureFromPath(captureScreenShot("ProbleminclickinginxPath"));
		}
	}
	public void jsClick(WebElement ele) throws IOException {
		String text = "";
		try {
			text = ele.getText();
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", ele);
			Reporter.addStepLog("The element "+text+" is clicked");
			Reporter.addScreenCaptureFromPath(captureScreenShot("Element clicked successfully"));
		} catch (InvalidElementStateException e) {
			Reporter.addStepLog("The element: "+text+" could not be clicked");
			Reporter.addScreenCaptureFromPath(captureScreenShot("ElementNotClicked"));
		} catch (WebDriverException e) {
			Reporter.addStepLog("Unknown exception occured while clicking in the field :");
			Reporter.addScreenCaptureFromPath(captureScreenShot("ElementNotClicked"));
		} 
	}


	public void MouseOver(WebElement ele1) throws IOException {
		String text = "";
		try {
			/*WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ele));*/
			text = ele1.getText();

			Actions builder = new Actions(driver);
			builder.moveToElement(ele1).pause(1000).build().perform();
			Reporter.addStepLog("Mouseover on the Element ::: " + text);
			Reporter.addScreenCaptureFromPath(captureScreenShot("ElementMouseover"));
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
			System.out.println(e.getMessage());
			Reporter.addStepLog(e.getMessage());
			Reporter.addStepLog("Problem in Mouseover on :::: " + text);
			Reporter.addScreenCaptureFromPath(captureScreenShot("ProbleminMouseover"));
		}
	}
	public void MouseOver(WebElement ele1, WebElement ele2) throws IOException {
		String text = "";
		try {
			/*WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ele));*/
			text = ele1.getText();

			Actions builder = new Actions(driver);
			builder.moveToElement(ele1).pause(1000).build().perform();
			builder.click(ele2).build().perform(); 	
			Reporter.addStepLog("Mouseover on the Element ::: " + text);
			Reporter.addScreenCaptureFromPath(captureScreenShot("ElementMouseover"));
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
			System.out.println(e.getMessage());
			Reporter.addStepLog(e.getMessage());
			Reporter.addStepLog("Problem in Mouseover on :::: " + text);
			Reporter.addScreenCaptureFromPath(captureScreenShot("ProbleminMouseover"));
		}
	}



	public void clickWithNoSnap(WebElement ele) throws IOException {
		String text = "";
		try {
			/*WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ele));*/	
			text = ele.getText();
			ele.click();			
			Reporter.addStepLog("The element "+text+" is clicked");
			Reporter.addScreenCaptureFromPath(captureScreenShot("Element clicked successfully"));
		} catch (InvalidElementStateException e) {
			Reporter.addStepLog("The element: "+text+" could not be clicked");
			Reporter.addScreenCaptureFromPath(captureScreenShot("ElementNotClicked"));
		} catch (WebDriverException e) {
			Reporter.addStepLog("Unknown exception occured while clicking in the field :");
			Reporter.addScreenCaptureFromPath(captureScreenShot("ElementNotClicked"));
		} 
	}

	public void selectDropDownUsingVisibleText(WebElement ele, String value) throws IOException {
		try {
			new Select(ele).selectByVisibleText(value);
			Reporter.addStepLog("The selected data: "+value+" entered successfully in the field :"+ele);
			Reporter.addScreenCaptureFromPath(captureScreenShot("DataEntered"));
		} catch (Exception e) {
			Reporter.addStepLog(e.getMessage());
			Reporter.addStepLog("Problem in Selecting Dropdown value");
			Reporter.addScreenCaptureFromPath(captureScreenShot("ProbleminSelectingDropdownvalue"));
		} 
	}
	public void selectDropDownUsingIndex(WebElement ele, int index) throws IOException {
		try {
			new Select(ele).selectByIndex(index);
			Reporter.addStepLog("The selected data: "+index+" entered successfully in the field :"+ele);
			Reporter.addScreenCaptureFromPath(captureScreenShot("DataEntered"));
		} catch (Exception e) {
			Reporter.addStepLog(e.getMessage());
			Reporter.addStepLog("Problem in Selecting Dropdown value");
			Reporter.addScreenCaptureFromPath(captureScreenShot("ProbleminSelectingDropdownvalue"));
		} 
	} 
	public void selectDropDownUsingValue(WebElement ele, String value) throws IOException {
		try {
			new Select(ele).selectByValue(value);
			Reporter.addStepLog("The dropdown is selected with text "+value);
			Reporter.addScreenCaptureFromPath(captureScreenShot("DataEntered"));
		} catch (WebDriverException e) {
			Reporter.addStepLog("The element: "+ele+" could not be found.");
			Reporter.addScreenCaptureFromPath(captureScreenShot("ProbleminSelectingDropdownvalue"));
		}
	}

	public void javaScriptSelectById(String locator, String datetxt) throws IOException {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("document.getElementByid(" + locator + ").value= '" + datetxt + "'");
			Reporter.addStepLog("Received the text");
			Reporter.addScreenCaptureFromPath(captureScreenShot("DropDonSelcted"));
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
			System.out.println(e.getMessage());
			Reporter.addStepLog(e.getMessage());
			Reporter.addStepLog("Problem in feeding text by javascript in:::: " + locator);
			Reporter.addScreenCaptureFromPath(captureScreenShot("Probleminsendingtextbyjavascriptid"));
		}
	}

	public void dragAndDrop(WebElement ele1, WebElement ele2) throws IOException {
		try {
			Actions builder = new Actions(driver);
			builder.dragAndDrop(ele1, ele2).perform();
			Reporter.addStepLog("The element dropped successfully");
			Reporter.addScreenCaptureFromPath(captureScreenShot("ElementDropped"));
		} catch (InvalidElementStateException e) {
			Reporter.addStepLog("The element not dropped successfully");
			Reporter.addScreenCaptureFromPath(captureScreenShot("ProblemWhileDropingElement"));
		} catch (WebDriverException e) {
			Reporter.addStepLog("Unknown exception occured while dragging the elemnt");
			Reporter.addScreenCaptureFromPath(captureScreenShot("ProblemWhileDropingElement"));
		}
	}

	public String getText(WebElement ele) throws IOException {	
		String bReturn = "";
		try {
			bReturn = ele.getText();
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
			System.out.println(e.getMessage());
			Reporter.addStepLog(e.getMessage());
			Reporter.addStepLog("Problem in getText xpath:::: " + bReturn);
			Reporter.addScreenCaptureFromPath(captureScreenShot("ProblemingetTextxpath"));
		}
		return bReturn;
	}
	public String getTitle() {		
		String bReturn = "";
		try {
			bReturn =  driver.getTitle();
		} catch (WebDriverException e) {
			Reporter.addStepLog("Unknown Exception Occured While fetching Title");
		} 
		return bReturn;
	}
	public String getAttribute(WebElement ele, String attribute) throws IOException {		
		String bReturn = "";
		try {
			bReturn=  ele.getAttribute(attribute);
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
			System.out.println(e.getMessage());
			Reporter.addStepLog(e.getMessage());
			Reporter.addStepLog("Problem in getAttribute xpath:::: " + attribute);
			Reporter.addScreenCaptureFromPath(captureScreenShot("ProblemingetAttributexpath"));
		}
		return bReturn; 
	} 

	public boolean verifyExactTitle(String title) {
		boolean bReturn =false;
		try {
			if(getTitle().equals(title)) {
				Reporter.addStepLog("The title of the page matches with the value :"+title);
				bReturn= true;
			}else {
				Reporter.addStepLog("The title of the page:"+driver.getTitle()+" did not match with the value :"+title);
			}
		} catch (WebDriverException e) {
			Reporter.addStepLog("Unknown exception occured while verifying the title");
		} 
		return bReturn;
	}

	public void verifyExactText(WebElement ele, String expectedText) throws IOException {
		try {
			if(getText(ele).equals(expectedText)) { 
				Reporter.addStepLog("The text: "+getText(ele)+" matches with the value :"+expectedText);
			}else {
				Reporter.addStepLog("The text "+getText(ele)+" doesn't matches the actual "+expectedText);
			}
		} catch (WebDriverException e) {
			Reporter.addStepLog("Unknown exception occured while verifying the Text");
		} 

	}
	public String verifyTwoStringValues(String exactText, String expectedText) {
		try {
			if(exactText.equals(expectedText)) {
				Reporter.addStepLog("The text: "+expectedText+" matches with the value :"+expectedText);
			}else {
				//Reporter.addStepLog("The text "+exactText+" doesn't matches the actual "+expectedText);
			} 
		} catch (WebDriverException e) {
			Reporter.addStepLog("Unknown exception occured while verifying the Text");
		} 
		return expectedText;
	}

	public void verifyPartialText(WebElement ele, String expectedText) throws IOException {
		try {
			String text = getText(ele);
			if(getText(ele).contains(expectedText)) {
				Reporter.addStepLog("The expected text contains the actual "+text);
			}else {
				Reporter.addStepLog("The expected text doesn't contain the actual "+text);
			}
		} catch (WebDriverException e) {
			Reporter.addStepLog("Unknown exception occured while verifying the Text");
		} 
	}

	public void verifyExactAttribute(WebElement ele, String attribute, String value) throws IOException {
		try {
			if(getAttribute(ele, attribute).equals(value)) {
				Reporter.addStepLog("The expected attribute :"+attribute+" value matches the actual "+value);
			}else {
				Reporter.addStepLog("The expected attribute :"+attribute+" value does not matches the actual "+value);
			}
		} catch (WebDriverException e) {
			Reporter.addStepLog("Unknown exception occured while verifying the Attribute Text");
		} 

	}

	public void verifyPartialAttribute(WebElement ele, String attribute, String value) throws IOException {
		try {
			if(getAttribute(ele, attribute).contains(value)) {
				Reporter.addStepLog("The expected attribute :"+attribute+" value contains the actual "+value);
			}else {
				Reporter.addStepLog("The expected attribute :"+attribute+" value does not contains the actual "+value);
			}
		} catch (WebDriverException e) {
			Reporter.addStepLog("Unknown exception occured while verifying the Attribute Text");
		}
	}

	public void verifySelected(WebElement ele) {
		try {
			if(ele.isSelected()) {
				Reporter.addStepLog("The element "+ele+" is selected");
			} else {
				Reporter.addStepLog("The element "+ele+" is not selected");
			}
		} catch (WebDriverException e) {
			Reporter.addStepLog("WebDriverException : "+e.getMessage());
		}
	}


	public void VerifyBeforeSelect(WebElement ele) {
		try {
			if(ele.isSelected()) {
				Reporter.addStepLog("The element "+ele+" is selected already");
			} else {
				ele.click();
				Reporter.addStepLog("The element "+ele+" is selected now");
			}
		} catch (WebDriverException e) {
			Reporter.addStepLog("WebDriverException : "+e.getMessage());
		}
	}


	public void VerifyBeforeUnSelect(WebElement ele) {
		try {
			if(ele.isSelected()) {				
				ele.click();
				Reporter.addStepLog("The element "+ele+" is unchecked now");
			} else {				
				Reporter.addStepLog("The element "+ele+" is unchecked by default");
			}
		} catch (WebDriverException e) {
			Reporter.addStepLog("WebDriverException : "+e.getMessage());
		}
	}

	public boolean verifySelectedBoolean(WebElement ele) {
		boolean selected = false;
		try {
			if(ele.isSelected()) {
				Reporter.addStepLog("The element "+ele+" is selected");
				selected = true;
			} else {
				Reporter.addStepLog("The element "+ele+" is not selected");
				selected = false;
			}
		} catch (WebDriverException e) {
			Reporter.addStepLog("WebDriverException : "+e.getMessage());
		}
		return selected;
	}
	public void verifyDisplayed(WebElement ele) {
		try {
			if(ele.isDisplayed()) {
				Reporter.addStepLog("The element "+ele+" is visible");
			} else {
				Reporter.addStepLog("The element "+ele+" is not visible");
			}
		} catch (WebDriverException e) {
			Reporter.addStepLog("WebDriverException : "+e.getMessage());
		} 
	}
	public boolean verifyPartialTitle(String title) {
		boolean bReturn =false;
		try {
			if(getTitle().contains(title)) {
				Reporter.addStepLog("The title of the page matches with the value :"+title);
				bReturn= true;
			}else {
				Reporter.addStepLog("The title of the page:"+driver.getTitle()+" did not match with the value :"+title);
			}
		} catch (WebDriverException e) {
			Reporter.addStepLog("Unknown exception occured while verifying the title");
		} 
		return bReturn;		
	}
	public boolean verifyEnabled(WebElement ele) {
		boolean enabled = false;
		try {
			if(ele.isEnabled()) {				
				enabled = true;
			} else {				
				enabled = false;
			}
		} catch (WebDriverException e) {
			Reporter.addStepLog("WebDriverException : "+e.getMessage());
		}
		return enabled; 
	}

	public static Set<String> allWindowHandles;
	public void switchToWindow(int index) throws IOException {
		try {
			allWindowHandles = driver.getWindowHandles();
			List<String> allHandles = new ArrayList<>();
			System.out.println("Opened window count : "+allWindowHandles.size()); 
			allHandles.addAll(allWindowHandles);
			driver.switchTo().window(allHandles.get(index));
		} catch (NoSuchWindowException e) {
			Reporter.addStepLog("The driver could not move to the given window by index "+index);
			Reporter.addScreenCaptureFromPath(captureScreenShot("WindowSwitched"));
		} catch (IndexOutOfBoundsException e) {
			Reporter.addStepLog("IndexOutOfBoundsException occured : "+index);
			Reporter.addScreenCaptureFromPath(captureScreenShot("ProblemToSwitchWindow"));
		} catch (WebDriverException e) {
			Reporter.addStepLog("WebDriverException : "+e.getMessage());
			Reporter.addScreenCaptureFromPath(captureScreenShot("ProblemToSwitchWindow"));
		}
	}

	public void switchToFrame(WebElement ele) throws IOException {
		try {
			driver.switchTo().frame(ele);
			Reporter.addStepLog("switch In to the Frame "+ele);
			Reporter.addScreenCaptureFromPath(captureScreenShot("SwitchedToFrame"));
		} catch (NoSuchFrameException e) {
			Reporter.addStepLog("WebDriverException : "+e.getMessage());
			Reporter.addScreenCaptureFromPath(captureScreenShot("ProblemToSwitchFrame"));
		} catch (WebDriverException e) {
			Reporter.addStepLog("WebDriverException : "+e.getMessage());
			Reporter.addScreenCaptureFromPath(captureScreenShot("ProblemToSwitchFrame"));
		} 
	}
	public void switchToFrame(int index) throws IOException {
		try {
			driver.switchTo().frame(index);
			Reporter.addStepLog("switch In to the Frame passed");
			Reporter.addScreenCaptureFromPath(captureScreenShot("SwitchedToFrame"));
		} catch (NoSuchFrameException e) {
			Reporter.addStepLog("WebDriverException : "+e.getMessage());
			Reporter.addScreenCaptureFromPath(captureScreenShot("ProblemToSwitchFrame"));
		} catch (WebDriverException e) {
			Reporter.addStepLog("WebDriverException : "+e.getMessage());
			Reporter.addScreenCaptureFromPath(captureScreenShot("ProblemToSwitchFrame"));
		} 
	}
	public void switchToFrame(String ele) throws IOException {
		try {
			driver.switchTo().frame(ele);
			Reporter.addStepLog("switch In to the Frame "+ele);
			Reporter.addScreenCaptureFromPath(captureScreenShot("SwitchedToFrame"));
		} catch (NoSuchFrameException e) {
			Reporter.addStepLog("WebDriverException : "+e.getMessage());
			Reporter.addScreenCaptureFromPath(captureScreenShot("ProblemToSwitchFrame"));
		} catch (WebDriverException e) {
			Reporter.addStepLog("WebDriverException : "+e.getMessage());
			Reporter.addScreenCaptureFromPath(captureScreenShot("ProblemToSwitchFrame"));
		} 
	}
	public void defaultContent() throws IOException {
		try {
			driver.switchTo().defaultContent();
			Reporter.addStepLog("comeout to the Frame ");
			Reporter.addScreenCaptureFromPath(captureScreenShot("ComeOutFrame"));
		} catch (NoSuchFrameException e) {
			Reporter.addStepLog("WebDriverException : "+e.getMessage());
			Reporter.addScreenCaptureFromPath(captureScreenShot("ProblemToSwitchFrame"));
		} catch (WebDriverException e) {
			Reporter.addStepLog("WebDriverException : "+e.getMessage());
			Reporter.addScreenCaptureFromPath(captureScreenShot("ProblemToSwitchFrame"));
		} 
	}

	public void acceptAlert() throws IOException {
		String text = "";		
		try {
			Alert alert = driver.switchTo().alert();
			text = alert.getText();
			alert.accept();
			Reporter.addStepLog("The alert "+text+" is accepted.");
			Reporter.addScreenCaptureFromPath(captureScreenShot("AlertAccepted"));
		} catch (NoAlertPresentException e) {
			Reporter.addStepLog("There is no alert present.");
			Reporter.addScreenCaptureFromPath(captureScreenShot("ProblemToAcceptAlert"));
		} catch (WebDriverException e) {
			Reporter.addStepLog("WebDriverException : "+e.getMessage());
			Reporter.addScreenCaptureFromPath(captureScreenShot("ProblemToAcceptAlert"));
		}  
	}

	public void dismissAlert() throws IOException {
		String text = "";		
		try {
			Alert alert = driver.switchTo().alert();
			text = alert.getText();
			alert.dismiss();
			Reporter.addStepLog("The alert "+text+" is dismissed.");
			Reporter.addScreenCaptureFromPath(captureScreenShot("AlertDismissed"));
		} catch (NoAlertPresentException e) {
			Reporter.addStepLog("There is no alert present.");
			Reporter.addScreenCaptureFromPath(captureScreenShot("ProblemToDismissAlert"));
		} catch (WebDriverException e) {
			Reporter.addStepLog("WebDriverException : "+e.getMessage());
			Reporter.addScreenCaptureFromPath(captureScreenShot("ProblemToDismissAlert"));
		} 

	}

	public String getAlertText() {
		String text = "";		
		try {
			Alert alert = driver.switchTo().alert();
			text = alert.getText();
		} catch (NoAlertPresentException e) {
			Reporter.addStepLog("There is no alert present.");
		} catch (WebDriverException e) {
			Reporter.addStepLog("WebDriverException : "+e.getMessage());
		} 
		return text;
	}

	public void scrollDown(String value) throws IOException, InterruptedException {
		try {
			Thread.sleep(3000);
			WebElement reg = locateElement("xpath", value);
			int y = reg.getLocation().getY();
			System.out.println("Location of y :"+ y); 
			Actions builder = new Actions(driver);
			builder.sendKeys(Keys.PAGE_DOWN).build().perform();
			/*((JavascriptExecutor) driver).executeScript("scroll(0,"+y+");");*/ 
			Thread.sleep(3000);
			Reporter.addStepLog("element scrolled"); 
			Reporter.addScreenCaptureFromPath(captureScreenShot("ScrollDown"));
		} catch (WebDriverException e) {
			Reporter.addStepLog("WebDriverException : "+e.getMessage());
			Reporter.addScreenCaptureFromPath(captureScreenShot("ProblemWithScrollDown"));
		}
	} 
	public void scrolldown() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}
	public void scrolltoview(String value) throws IOException {
		try {
			JavascriptExecutor je = (JavascriptExecutor) driver;
			// Identify the WebElement which will appear after scrolling down
			WebElement element = locateElement("xpath", value);
			// now execute query which actually will scroll until that element is
			// not appeared on page.
			je.executeScript("arguments[0].scrollIntoView(true);", element);
			Reporter.addStepLog("Element scrolled");
			Reporter.addScreenCaptureFromPath(captureScreenShot("ScrollDown"));
		} catch (WebDriverException e) {
			Reporter.addStepLog("WebDriverException : "+e.getMessage());
			Reporter.addScreenCaptureFromPath(captureScreenShot("ProblemWithScrollDown"));
		}
	}
	public void scroll() {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("scroll(0, 650);");
	}

	public void invisibilityOfWebElement(String ele) throws InterruptedException{
		try {
			Thread.sleep(500);
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.invisibilityOf(driver.findElementByXPath(prop.getProperty(ele))));
			Reporter.addStepLog("Element invisible successfully");
		} catch (WebDriverException e) { 
			Reporter.addStepLog("Thread time out");  
		} 
	}

	public void closeActiveBrowser() {
		try {
			driver.close();
			Reporter.addStepLog("The browser is closed");
		} catch (Exception e) {
			Reporter.addStepLog("The browser could not be closed");
		}
	}

	public void closeAllBrowsers() {
		try {
			driver.quit();
			Reporter.addStepLog("The opened browsers are closed");
		} catch (Exception e) {
			Reporter.addStepLog("Unexpected error occured in Browser");
		}
	}	
	public String captureScreenShot(String scenario) throws IOException {
		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		File f1 = null;
		String filename = null;
		if (environment.equals("local")) {
			f1 = new File(System.getProperty("user.dir") + "\\src\\test\\java\\snaps\\");
			if (f1.exists()) {
				// System.err.println("folder available");
			} else {
				f1.mkdirs();
				// System.out.println("Folder Created");
			}
			filename = path + "\\src\\test\\java\\snaps\\" + scenario + "_" + System.currentTimeMillis() + ".jpg";
		} else if (environment.equals("AWS")) {
			f1 = new File(System.getProperty("user.dir") + "//output//");
			if (f1.exists()) {
				// System.err.println("folder available");
			} else {
				f1.mkdirs();
				// System.out.println("Folder Created");
			}
			filename = "output//snaps//" + scenario + "_" + System.currentTimeMillis() + ".jpg";
		}
		FileUtils.copyFile(src, new File(filename));
		filename = filename.replace("output//", "");
		return filename;
	}

	//-------------------------------------------------------------------------------------------------------------
	//--------------------------------------------------------------------------------------------------------------	
	@SuppressWarnings("deprecation")
	public String readData(int coloum, int rows, String sheetname) throws IOException {
		String data = null;
		Workbook wb = null;
		String fileName = "";
		if (environment.equals("local")) {
			// Use below path for local
			fileName = path + "\\src\\test\\java\\data\\DataSheet.xlsx";
		} else if (environment.equals("AWS")) {
			// Use below path for AWS Linux
			fileName = path + "//test-classes//DataSheet.xlsx";
		}
		// System.err.println("File path is : "+fileName);
		FileInputStream fileInputStream = new FileInputStream(fileName);
		// fileInputStream.reset();
		String fileExtensionName = fileName.substring(fileName.indexOf("."));
		// System.err.println("File Extension : "+fileExtensionName);
		if (fileExtensionName.equals(".xlsx")) {
			wb = new XSSFWorkbook(fileInputStream);
			XSSFFormulaEvaluator.evaluateAllFormulaCells(wb);
		} else if (fileExtensionName.equals(".xls")) {
			wb = new HSSFWorkbook(fileInputStream);
			HSSFFormulaEvaluator.evaluateAllFormulaCells(wb);
		}
		Sheet sheet = wb.getSheet(sheetname);	

		Row row = sheet.getRow(rows);
		Cell cell = row.getCell(coloum);
		cell.setCellType(Cell.CELL_TYPE_STRING);

		if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
			System.out.println("string: " + cell.getStringCellValue());
			data = cell.getStringCellValue();
		}
		if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
			System.out.println("numeric: " + cell.getNumericCellValue());
		}
		System.out.println("any: " + cell.toString());
		data = cell.toString();
		return data;
	}

	public String readDataByColumnName(String sheetname, String columnName, int rowNum) throws IOException {
		String data = null;
		Workbook wb = null;
		String fileName = "";
		if (environment.equals("local")) {
			// Use below path for local
			fileName = path + "\\src\\test\\java\\data\\DataSheet.xlsx";
		} else if (environment.equals("AWS")) {
			// Use below path for AWS Linux
			fileName = path + "//test-classes//DataSheet.xlsx";
		}
		// System.err.println("File path is : "+fileName);
		FileInputStream fileInputStream = new FileInputStream(fileName);
		// fileInputStream.reset();
		String fileExtensionName = fileName.substring(fileName.indexOf("."));
		// System.err.println("File Extension : "+fileExtensionName);
		if (fileExtensionName.equals(".xlsx")) {
			wb = new XSSFWorkbook(fileInputStream);
			XSSFFormulaEvaluator.evaluateAllFormulaCells(wb);
		} else if (fileExtensionName.equals(".xls")) {
			wb = new HSSFWorkbook(fileInputStream);
			HSSFFormulaEvaluator.evaluateAllFormulaCells(wb);
		}
		Sheet sheet = wb.getSheet(sheetname);

		Row row = sheet.getRow(0);
		int lastColumn = row.getLastCellNum();

		int columnNum = 0;

		for(int i=0; i<=lastColumn; i++)
		{

			Cell cell = row.getCell(i);
			String tempText = cell.getStringCellValue();

			if(tempText.equals(columnName))
			{
				columnNum = i;
				break;
			}			

		}

		row = sheet.getRow(rowNum);
		Cell cell = row.getCell(columnNum);

		//Row row = sheet.getRow(rows);
		//Cell cell = row.getCell(coloum);
		cell.setCellType(Cell.CELL_TYPE_STRING);

		if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
			System.out.println("string: " + cell.getStringCellValue());
			data = cell.getStringCellValue();
		}
		if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
			System.out.println("numeric: " + cell.getNumericCellValue());
		}
		System.out.println("any: " + cell.toString());
		data = cell.toString();
		return data;
	}

	@SuppressWarnings({ "null", "deprecation" })
	public void writeDataWithColour(int coloum, int rows, String text, String sheetname, String color)
			throws EncryptedDocumentException, InvalidFormatException, IOException {
		Workbook wb1 = null;
		Sheet sheet;
		Cell cell;
		Row row;
		CellStyle style = null;
		String fileName = "";
		if (environment.equals("local")) {
			// Use below path for local
			fileName = path + "\\src\\test\\java\\data\\DataSheet.xlsx";
		} else if (environment.equals("AWS")) {
			// Use below path for AWS Linux
			fileName = path + "//test-classes//DataSheet.xlsx";
		}
		FileInputStream fileInputStream = new FileInputStream(fileName);
		String fileExtensionName = fileName.substring(fileName.indexOf("."));
		if (fileExtensionName.equals(".xlsx")) {
			wb1 = new XSSFWorkbook(fileInputStream);
			XSSFFormulaEvaluator.evaluateAllFormulaCells(wb1);
			style = wb1.createCellStyle();

			Font font = wb1.createFont();
			if (color.equalsIgnoreCase("Green")) {
				font.setColor(IndexedColors.GREEN.getIndex());
			} else if (color.equalsIgnoreCase("Red")) {
				font.setColor(IndexedColors.RED.getIndex());
			} else {
				font.setColor(IndexedColors.BLACK.getIndex());
			}
			style.setFont(font);
		} else if (fileExtensionName.equals(".xls")) {
			wb1 = new HSSFWorkbook(fileInputStream);
			HSSFFormulaEvaluator.evaluateAllFormulaCells(wb1);
			//	style = wb1.createCellStyle();
			Font font = wb1.createFont();
			if (color.equalsIgnoreCase("Green")) {
				font.setColor(HSSFColor.GREEN.index);
			} else if (color.equalsIgnoreCase("Red")) {
				font.setColor(HSSFColor.RED.index);
			} else {
				font.setColor(HSSFColor.BLACK.index);
			}
			//	style.setFont(font);
		}
		if (wb1 == null) {
			sheet = wb1.createSheet();
		}
		sheet = wb1.getSheet(sheetname);
		row = sheet.getRow(rows);
		if (row == null) {
			row = sheet.createRow(rows);
		}
		cell = row.getCell(coloum);
		if (cell == null)
			cell = row.createCell(coloum);
		cell.setCellType(Cell.CELL_TYPE_STRING);
		cell.setCellValue(text);
		//	cell.setCellStyle(style);
		FileOutputStream fileOut = null;
		if (environment.equals("local")) {
			// Use below path for local
			fileOut = new FileOutputStream(path + "\\src\test\\java\\data\\DataSheet.xlsx");
		} else if (environment.equals("AWS")) {
			// Use below path for AWS Linux
			fileOut = new FileOutputStream(path + "//test-classes//DataSheet.xlsx");
		}
		wb1.write(fileOut);
		fileOut.close();
	}

	@SuppressWarnings({ "null", "deprecation" })
	public void writeData(int coloum, int rows, String text, String sheetname)
			throws EncryptedDocumentException, InvalidFormatException, IOException {
		Workbook wb1 = null;
		Sheet sheet;
		Cell cell;
		Row row;
		//CellStyle style = null;
		String fileName = "";
		if (environment.equals("local")) {
			// Use below path for local
			fileName = path + "\\src\\test\\java\\data\\DataSheet.xlsx";
		} else if (environment.equals("AWS")) {
			// Use below path for AWS Linux
			fileName = path + "//test-classes//DataSheet.xlsx";
		}
		FileInputStream fileInputStream = new FileInputStream(fileName);
		String fileExtensionName = fileName.substring(fileName.indexOf("."));
		if (fileExtensionName.equals(".xlsx")) {
			wb1 = new XSSFWorkbook(fileInputStream);
			XSSFFormulaEvaluator.evaluateAllFormulaCells(wb1);			
		} else if (fileExtensionName.equals(".xls")) {
			wb1 = new HSSFWorkbook(fileInputStream);
			HSSFFormulaEvaluator.evaluateAllFormulaCells(wb1);			
		}
		if (wb1 == null) {
			sheet = wb1.createSheet();
		}
		sheet = wb1.getSheet(sheetname);
		row = sheet.getRow(rows);
		if (row == null) {
			row = sheet.createRow(rows);
		}
		cell = row.getCell(coloum);
		if (cell == null)
			cell = row.createCell(coloum);
		cell.setCellType(Cell.CELL_TYPE_STRING);
		cell.setCellValue(text);
		//	cell.setCellStyle(style);
		FileOutputStream fileOut = null;
		if (environment.equals("local")) {
			// Use below path for local
			fileOut = new FileOutputStream(path + "\\src\\test\\java\\data\\DataSheet.xlsx");
		} else if (environment.equals("AWS")) {
			// Use below path for AWS Linux
			fileOut = new FileOutputStream(path + "//test-classes//DataSheet.xlsx");
		}
		wb1.write(fileOut);
		fileOut.close();
	}


	public void uploadfile(WebElement ele, String Pathtxt) throws Exception {

		click(ele); 
		// Copy your file's absolute path to the clipboard
		StringSelection ss = new StringSelection(Pathtxt);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);

		// Paste the file's absolute path into the File name field of the File
		// Upload dialog box
		// native key strokes for CTRL, V and ENTER keys
		Robot robot = new Robot();

		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.delay(5000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
	}

	public void errorMessage(WebElement element) throws Exception {
		String errorMesage = element.getText(); 
		try {
			System.err.println("ERROR MESSAGE ====> " + errorMesage);
			Reporter.addStepLog(errorMesage);
			// Reporter.addScreenCaptureFromPath(captureScreenShot(errorMesage));
			Assert.assertEquals(true, true);
		} catch (Exception e) {
			System.err.println("ERROR MESSAGE " + e);
			System.err.println("");
			Assert.assertEquals(true, false);
		}
	}

	public void errorMessageTrainer(List<WebElement> elements) throws Exception {
		int size =elements.size(); 
		try {
			if (size > 1) {
				Reporter.addStepLog("Error Prompt Shown");
				Assert.assertEquals(true, true);
			} else {
				System.err.println("Error Prompt Not Shown");
				Assert.assertEquals(true, false);
			}
		} catch (Exception e) {
			System.err.println("ERROR MESSAGE " + e);
			System.err.println("Error Prompt Not Shown");
			Assert.assertEquals(true, false);
		}
	}

	// ====================================================================================================================================

	public void selectMultiplePermissionList(List<WebElement> elements, String result) throws Exception {
		String[] split = result.split(",");
		int length = split.length;
		System.out.println("LENGTH == " + length); 
		System.out.println(split[0] + "<=========> " + split[1]);
		for (int i = 0; i < split.length; i++) {
			for (int j = 1; j <= elements.size(); j++) {
				String permissions = elements.get(j).getText();
				System.out.println("**********************************************");
				System.out.println(i + " . " + split[i] + " == " + j + " . " + permissions);
				System.out.println("**********************************************");
				if (split[i].equalsIgnoreCase(permissions)) {
					WebElement ele = driver
							.findElements(By.xpath("//input[@type = 'checkbox'] [@name = 'permissions[]']")).get(j);
					if (!ele.isSelected()) {
						ele.click();
					}
					Thread.sleep(2000);
					break;
				} else {
					continue;
				}
			}
		}
	}

	public void elementNotVisible(WebElement element) throws Exception {
		boolean result = true;
		try {
			result = element.isDisplayed();
		} catch (Exception e) {
			result = false;
		}
		if (result == false) {
			System.err.println("Element Not Visible");
			Reporter.addStepLog("elementNotVisible");
			Reporter.addScreenCaptureFromPath(captureScreenShot("elementNotVisible"));
			Assert.assertEquals(true, true);
		} else {
			System.err.println("Element Visible");
			Reporter.addStepLog("elementVisible");
			Reporter.addScreenCaptureFromPath(captureScreenShot("elementVisible"));
			Assert.assertEquals(true, false);
		}
	}

	public void clickActionDropdown(String xPath) throws Exception {
		try {
			String text = driver.findElementByXPath(prop.getProperty(xPath)).getText();
			System.out.println("BUTTON TEXT IS ==> " + text);
			WebElement element = driver.findElement(By.xpath(xPath));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", element);
			Reporter.addStepLog("Click on  the Element ::: " + xPath);
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
			System.out.println(e.getMessage());
			Reporter.addStepLog(e.getMessage());
			Reporter.addStepLog("Problem in clicking in xPath:::: " + xPath);
			Reporter.addScreenCaptureFromPath(captureScreenShot("ProbleminclickinginxPath"));
		}
	}

	public void waitUntillinvisible(String name) {
		WebDriverWait wait1 = new WebDriverWait(driver, 250);
		wait1.until(ExpectedConditions.invisibilityOfElementLocated(By.name(prop.getProperty(name))));
	}

	public void verifyDropDownOption(WebElement ele1, List<WebElement> ele2) throws Exception {
		System.err.println("Entered into DropDown");
		int count = 0;
		click(ele1); 
		for (WebElement element : ele2) {
			if (element.getText().equalsIgnoreCase("Begginer") || element.getText().equalsIgnoreCase("Intermediate")
					|| element.getText().equalsIgnoreCase("Advanced")) {
				count = count + 1;
			}
		}
		if (count == 3) {
			System.err.println("Fields are dispalying");
			Reporter.addStepLog("fieldsDisplaying");
			Reporter.addScreenCaptureFromPath(captureScreenShot("fieldsDisplaying"));
			Assert.assertEquals(true, true);
		} else {
			System.err.println("Fields are not dispalying");
			Reporter.addStepLog("fieldsNotDisplaying");
			Reporter.addScreenCaptureFromPath(captureScreenShot("fieldsNotDisplaying"));
			Assert.assertEquals(true, false);
		}
	}

	public void waitUntilPageloads() throws InterruptedException {
		ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver jse) {
				return ((JavascriptExecutor) jse).executeScript("return document.readyState").toString()
						.equals("complete");
			}
		};
		ExpectedCondition<Boolean> expectation1 = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver jse) {
				return ((JavascriptExecutor) jse).executeScript("return event.type").toString().equals("load");
			}
		};
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			try {
				wait.until(expectation);
				wait.until(expectation1);
			} catch (Exception e) {
				System.err.println("Captured");
			}
		} catch (Throwable error) {
			Assert.fail("Timeout waiting for Page Load Request to complete.");
		}

	}
	public void wait(String millis) throws InterruptedException {
		/* To wait until page loads */
		waitUntilPageloads();
		Thread.sleep(Long.parseLong(millis));
	}

	@SuppressWarnings("unused")
	public void uploadMoreFileAutoit(String xpath, String fileDestination)
			throws AWTException, IOException, InterruptedException {
		try {
			wait("2000");
			Thread.sleep(2000);
			int size = driver.findElements(By.xpath(prop.getProperty(xpath))).size();
			for (int i = size; i > 0; i--) {
				System.err.println("Value of size :: " + size);
				driver.findElements(By.xpath(prop.getProperty(xpath))).get(size - 1).click();
				break;
			}
			wait("2000");
			StringSelection ss = new StringSelection(fileDestination);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
			Runtime.getRuntime().exec(System.getProperty("user.dir") + "\\Datadriven\\FileUpload\\FileUpload.exe");
			wait("15000");
			System.err.println("File Uploaded Successfully");
			Reporter.addStepLog("fileUploadedSuccessfully");
			Reporter.addScreenCaptureFromPath(captureScreenShot("fileUploadedSuccessfully"));
		} catch (Exception e) {
			System.err.println("Error Msg :: " + e.getMessage());
			System.err.println("File Not Uploaded Successfully");
			Reporter.addStepLog("fileNotUploadedSuccessfully");
			Reporter.addScreenCaptureFromPath(captureScreenShot("fileNotUploadedSuccessfully"));
			Assert.assertEquals(true, false);
		}
	}

	@SuppressWarnings("unused")
	public void scrollToViewMore(String xpath) {
		int size = driver.findElements(By.xpath(prop.getProperty(xpath))).size();
		JavascriptExecutor je = (JavascriptExecutor) driver;
		for (int i = size; i > 0; i--) {
			System.err.println("Value of size :: " + size);
			WebElement element = driver.findElements(By.xpath(prop.getProperty(xpath))).get(size - 1);
			je.executeScript("arguments[0].scrollIntoView(true);", element);
			break;
		}
	}

	public void verifyDate(WebElement ele, String date) throws Exception {
		String text = ele.getAttribute("value");
		if (text.trim().equalsIgnoreCase(date.trim())) {
			System.err.println(text + " == " + date);
			System.err.println("Current date dispalyed in start date field");
			Reporter.addStepLog("Current date dispalyed in start date field");
			Reporter.addScreenCaptureFromPath(captureScreenShot("currentDateTimeDisplayed"));
			Assert.assertEquals(true, true);
		} else {
			System.err.println(text + " == " + date);
			System.err.println("Current date not dispalyed in start date field");
			Reporter.addStepLog("Current date not dispalyed in start date field");
			Reporter.addScreenCaptureFromPath(captureScreenShot("currentDateTimeNotDisplayed"));
			Assert.assertEquals(true, false);
		}
	}
	public void clickUntilWaitElementPresent(String xpath) {
		WebDriverWait wait = new WebDriverWait(driver, 500);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(prop.getProperty(xpath)))).click();
	}

	public void switchWindows() {
		Set<String> winName = driver.getWindowHandles();
		System.err.println(winName.size());
		for (String window : winName) {
			System.err.println("Window name :: " + window);
			driver.switchTo().window(window);
		}
	}
	public void switchWindow() {
		Set<String> winName = driver.getWindowHandles();
		String firstWindow = driver.getWindowHandle();
		System.err.println(winName.size());
		for (String window : winName) {
			System.err.println("Window name :: " + window);
			if(window != firstWindow){
				System.err.println("Second Window name :: " + window);
				driver.switchTo().window(window);
			}
		}
	}

	public void verifyElementInList(List<WebElement> options, String inputText) throws Exception {
		System.err.println("Options size :: "+options.size());
		for (WebElement option : options) {
			if (option.getText().trim().equalsIgnoreCase(inputText.trim())) {
				if (option.isDisplayed()) {
					System.err.println(option.getText().trim() + " == " + inputText.trim());
					System.err.println("Element Present in the List");
					Reporter.addStepLog("Element Present in the List");
					Reporter.addScreenCaptureFromPath(captureScreenShot("elementPresentInTheList"));
					break;
				} else {
					System.err.println("Element Not Present in the List");
					Reporter.addStepLog("Element Not Present in the List");
					Reporter.addScreenCaptureFromPath(captureScreenShot("elementNotPresentInTheList"));
					Assert.assertEquals(true, false);
				}
			} else {
				continue;
			}
		}

	}

	public void verifyList(List<WebElement> list) throws Exception {
		if (list.size() > 0) {
			System.err.println("Elements listed Successfully");
			Reporter.addStepLog("Elements listed Successfully");
			Reporter.addScreenCaptureFromPath(captureScreenShot("elementsListedSuccessfully"));
		} else {
			System.err.println("Elements Not listed Successfully");
			Reporter.addStepLog("Elements Not listed Successfully");
			Reporter.addScreenCaptureFromPath(captureScreenShot("elementsNotListedSuccessfully"));
			Assert.assertEquals(true, false);
		}
	}

	public void verifyElementInListAndClick(List<WebElement> options, String inputText) throws Exception {
		try {
			System.err.println("Options size :: "+options.size());
			for (WebElement option : options) {
				if (option.getText().trim().equalsIgnoreCase(inputText.trim())) {
					option.click();
					System.err.println("Clicked the element");
					Reporter.addStepLog("Clicked the element");
					Reporter.addScreenCaptureFromPath(captureScreenShot("clickedTheElement"));
					break;
				} else {
					continue;
				}
			}
		} catch (Exception e) {
			System.err.println("Problem in clicking the element :: " + e.getMessage());
			Reporter.addStepLog("Problem in clicking the element");
			Reporter.addScreenCaptureFromPath(captureScreenShot("ProblemInClickingTheElement"));
			Assert.assertEquals(true, false);
		}

	}

	public void ClickElementList(String xpath, String inputText) throws Exception {
		try {
			int size = driver.findElements(By.xpath(prop.getProperty(xpath))).size();
			System.err.println("Clicked Size :: "+size);
			for (int i = 1; i <= size; i++) { 
				String courseName = driver.findElement(By.xpath(prop.getProperty(xpath + "[" + i + "]"))).getText();
				//	System.err.println("Clicked_B :: " + "   " + i + "   " + courseName);
				System.err.println(courseName.trim()+" == "+inputText.trim());
				if (courseName.trim().equalsIgnoreCase(inputText.trim())) {
					System.err.println(courseName.trim() + " ::==:: " + inputText.trim());
					System.err.println("driver.findElement(By.xpath("+ xpath +"[" + i + "]");
					driver.findElement(By.xpath(xpath + "[" + i + "]")).click();

					System.err.println("Clicked detail btn");
					break;
				} else {
					continue;
				}
			}
		} catch (Exception e) {
			System.err.println("Problem in clicking the element :: " + e.getMessage());
			Reporter.addStepLog("Problem in clicking the element");
			Reporter.addScreenCaptureFromPath(captureScreenShot("ProblemInClickingTheElement"));
		}

	}
	public void dateSelection(String data, String sDate, String eDate) throws Exception{		
		//click(locateElement("xpath", data));	
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		Date givenSdate = dateFormat.parse(sDate);
		//get start Date month year seperately in dd mmm yyyy formats seperately
		String setSdate = new SimpleDateFormat("dd").format(givenSdate);
		String setSmonth = new SimpleDateFormat("MMM").format(givenSdate);		
		String setSyear = new SimpleDateFormat("yyyy").format(givenSdate);
		int sYear = Integer.parseInt(setSyear);		

		Date givenEdate = dateFormat.parse(eDate);
		//get end Date month year seperately in dd mmm yyyy formats seperately
		String setEdate = new SimpleDateFormat("dd").format(givenEdate); 
		String setEmonth = new SimpleDateFormat("MMM").format(givenEdate);		
		String setEyear = new SimpleDateFormat("yyyy").format(givenEdate);
		int eYear = Integer.parseInt(setEyear);	

		String displayedMY1 = getText(locateElement("xpath", "//th[@class='month']"));
		String [] disp1 = displayedMY1.split(" "); int dispYear1 =  Integer.parseInt(disp1[1]);

		// Select Start Date
		while((sYear<dispYear1)||(!displayedMY1.equalsIgnoreCase(setSmonth+" "+setSyear)))
		{
			locateElement("xpath", "//th[@class='prev available']/i").click();
			displayedMY1 = getText(locateElement("xpath", "//th[@class='month']"));								
			disp1 = displayedMY1.split(" ");
			dispYear1 =  Integer.parseInt(disp1[1]);			
		}
		List<WebElement> DatesList1 = driver.findElements(By.xpath("//th[text()='"+setSmonth+" "+setSyear+"']/../../../tbody/tr/td"));

		for(WebElement Date: DatesList1)
		{
			String dateElement = Date.getText();
			String dateElementClass = Date.getAttribute("class");
			if(dateElement.length()==1){dateElement="0"+dateElement;}
			if((dateElement.equalsIgnoreCase(setSdate))&&(dateElementClass.equalsIgnoreCase("available")))
			{				
				Date.click();
				Thread.sleep(5000);				
				break;
			}
		}
		// Select End Date
		String displayedMY2 = getText(locateElement("xpath", "(//th[@class='month'])[2]"));
		String [] disp2 = displayedMY2.split(" "); int dispYear2 =  Integer.parseInt(disp2[1]);

		while(((dispYear1<eYear)&&(dispYear2<eYear))||((!displayedMY1.equalsIgnoreCase(setEmonth+" "+setEyear))&&
				(!displayedMY2.equalsIgnoreCase(setEmonth+" "+setEyear))))
		{
			locateElement("xpath", "//th[@class='next available']/i").click();
			displayedMY1 = getText(locateElement("xpath", "//th[@class='month']"));								
			disp1 = displayedMY1.split(" ");
			dispYear1 =  Integer.parseInt(disp1[1]);
			displayedMY2 = getText(locateElement("xpath", "(//th[@class='month'])[2]"));								
			disp2 = displayedMY2.split(" ");
			dispYear2 =  Integer.parseInt(disp2[1]);			
		}

		List<WebElement> DatesList2 = driver.findElements(By.xpath("//th[text()='"+setEmonth+" "+setEyear+"']/../../../tbody/tr/td"));

		for(WebElement Date: DatesList2)
		{
			String dateElement = Date.getText();
			String dateElementClass = Date.getAttribute("class");
			if(dateElement.length()==1){dateElement="0"+dateElement;}
			if((dateElement.equalsIgnoreCase(setEdate))&&(dateElementClass.equalsIgnoreCase("available")))
			{
				Date.click();
				Thread.sleep(5000);				
				break;
			}
		}
	}

	public void verifyTextEquals(WebElement ele,String inputText) throws Exception
	{
		String exactText = ele.getText();
		if(exactText.trim().equalsIgnoreCase(inputText.trim())){
			System.err.println("Texts are equals");
		} else {
			System.err.println("Texts are not equals");
		}
	}

	@SuppressWarnings("deprecation")
	public void fluentWait()
	{
		@SuppressWarnings("unused")
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)							
		.withTimeout(30, TimeUnit.SECONDS) 			
		.pollingEvery(5, TimeUnit.SECONDS) 			
		.ignoring(NoSuchElementException.class);	
	}



	public void inputComboTextidPagination(String comboname, String inputText) throws Exception{

		boolean status = false;
		int loopcnt = 0;
		freshStart: do {

			//To quit after 5 attempts
			if (loopcnt++ == 25)
				return;
			// System.err.println("comboname :: "+comboname);
			// To enter & click 
			type(driver.findElementByXPath("//*[@id='" + comboname.trim() + "']/../span/input[1]"), inputText);

			wait("2000");
			List<WebElement> dataGrids = driver.findElements(By.xpath("//*[contains(@class,'datagrid-wrap') and contains(@class,'panel-body-noheader')]"));
			//System.out.println("dataGrids: " + dataGrids.size());
			try {
				for (WebElement dataGrid : dataGrids) {
					if (dataGrid.isDisplayed()) {
						WebElement btable = dataGrid.findElement(By.className("datagrid-btable"));
						List<WebElement> divs = btable.findElements(By.tagName("div"));
						for (WebElement div : divs) {
							//System.err.println(div.getAttribute("innerText"));
							if (div.getAttribute("innerHTML").trim().equalsIgnoreCase(inputText.trim())) {
								click(div);
								//div.click();
								status = true;
								continue freshStart;
							}
						}
						//To click next-page
						List<WebElement> lst = dataGrid.findElements(By.xpath("//*[contains(@class,'pagination-next')]"));
						/*ListIterator<WebElement> ite = lst.listIterator(lst.size());
						while (ite.hasPrevious()) {
							ite.previous().click();
							continue freshStart;
						}*/
						for (WebElement webElement : lst) {
							if (webElement.isDisplayed()) {
								webElement.click();
								continue freshStart;
							}
						}
					}
				}

			} catch (StaleElementReferenceException e) {
				continue freshStart;
			}			
		} while (!status);
		wait("3000");

	}



}
