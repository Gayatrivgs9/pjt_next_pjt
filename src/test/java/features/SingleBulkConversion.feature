Feature: GSK Smoke TestCases 

Background: 
	Given Launch the browser and Load the DP Url 
	And Enter Credentials and login1 
	
Scenario: Login to DP and Create Sales Order 
	And Mouseover on SalesManagement Tab 
	And Click Sales Order 
	Then Click Add Sales order button 
	And Enter required Sales Order details from dataSheet1 
	And Validate the product tatal price from dataSheet1 
	
Scenario: Convert the two Sales Order by single click in bulk order 
	And Mouseover on SalesManagement Tab 
	And Click Bulk Order Conversion 
	And Click on populate button 
	And Select the required Sales Order 
	And Click on Single click bulk conversion 
	And Click on Credit Bills 
	
Scenario: Verify the sales invoice which convert from the Bulk Order 
	And Mouseover on SalesManagement Tab 
	And Click Sales Invoice