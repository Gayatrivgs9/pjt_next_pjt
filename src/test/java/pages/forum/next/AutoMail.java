package pages.forum.next;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.cucumber.listener.Reporter;

import cucumber.api.java.en.Given;
import main.MainClass;

public class AutoMail {	

	
	@Given("Write output in a notepad and change the Status")
	public void Change_Status() throws Throwable {
		MainClass.newNotePad=true;		
		System.out.println("Second scenario");
	}

	@Given("Send an email to Dev team with output attached")
	public void Send_AutoMail() throws Throwable {

		// Create object of Property file
		Properties props = new Properties();
		// this will set host of server- you can change based on your requirement 
		props.put("mail.smtp.host", "smtp.gmail.com");
		// set the port of socket factory 
		//AWS
		props.put("mail.smtp.socketFactory.port", "587");
		//local
		//props.put("mail.smtp.socketFactory.port", "465");
		// set socket factory
		props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
		// set the authentication to true
		props.put("mail.smtp.auth", "true");
		// set the port of SMTP server
		//AWS
		props.put("mail.smtp.port", "587");
		// local
		//props.put("mail.smtp.port", "465");
		props.put("mail.smtp.starttls.enable", "true");
		//props.put("mailAltConfig", "true");
		// This will handle the complete authentication
		Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("livewire.sify@gmail.com", "sifywire");
				//return new PasswordAuthentication("livewire.helpdesk@sifycorp.com", "sifywire");
			}
		});
		try {
			// Create object of MimeMessage class
			Message message = new MimeMessage(session);
			// Set the from address
			message.setFrom(new InternetAddress("livewire.sify@gmail.com"));
			//	message.setFrom(new InternetAddress("livewire.helpdesk@sifycorp.com"));
			// Set the recipient address
			//	message.setRecipients(Message.RecipientType.TO,InternetAddress.parse("anishkumar.kuppusamy@sifycorp.com"));
			message.setRecipients(Message.RecipientType.TO,InternetAddress.parse("gayatri.kolusu@sifycorp.com,kamalesh.dhayalan@sifycorp.com"));
			message.setRecipients(Message.RecipientType.CC,InternetAddress.parse("gayatri429233@gmail.com"));
			//Add the subject link
			Date date = new Date();  
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  
			String strDate= formatter.format(date);  
			System.out.println(strDate);  
			message.setSubject("LiveWire Smoke Test Report "+strDate);
			//Set content
			BodyPart messageBodyPart3 = new MimeBodyPart();
			messageBodyPart3.setText("Hi Team,"+"\n"+"\n"+"Kindly find the summary of the execution as follows"+"\n"+"Total Scenarios Passed: "+MainClass.passCount +"\n"+"Total Scenarios Failed: "+MainClass.failCount+"\n"+"\n"+"Regards,"+"\n"+"LiveWire Automation");
			//	message.setContent("<h1>This is actual message embedded in HTML tags</h1>","text/html");
			//	message.setContent("<h1>This is actual message embedded in HTML tags</h1>","text/html");
			// Create object to add multimedia type content
			//BodyPart messageBodyPart1 = new MimeBodyPart();
			BodyPart messageBodyPart2 = new MimeBodyPart();
			// Set the body of email
			//	messageBodyPart1.setText("Hi Team");
			//	messageBodyPart1.setText("Total Scenarios Passed: "+UseCukeFromMain.passCount +"\n"+"Total Scenarios Failed: "+UseCukeFromMain.failCount);
			// Create another object to add another content
			MimeBodyPart messageBodyPart4 = new MimeBodyPart();
			messageBodyPart4.setText("Total Scenarios Passed: "+MainClass.passCount +"\n"+"Total Scenarios Failed: "+MainClass.failCount);
			// Mention the file which you want to send
			String filename = "output//output_"+Hooks.fileExtn+".txt";
			// Create data source and pass the filename
			DataSource source = new FileDataSource(filename);
			// set the handler
			messageBodyPart2.setDataHandler(new DataHandler(source));
			// set the file
			filename = filename.substring(6);
			messageBodyPart2.setFileName(filename);
			// Create object of MimeMultipart class
			Multipart multipart = new MimeMultipart();
			// add body part 1
			multipart.addBodyPart(messageBodyPart2);
			// add body part 2
			multipart.addBodyPart(messageBodyPart3);
			// set the content
			message.setContent(multipart);
			// finally send the email
			Transport.send(message);
			System.out.println("=====Email Sent=====");
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
		Reporter.addStepLog("Auto Mail Sent");
		MainClass.newNotePad=false;	
	}


}
