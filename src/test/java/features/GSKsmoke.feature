Feature: GSK Smoke TestCases

@smoke1
Scenario: Login to DP and Create BEAT
   Given Clear the Beat Excel Values1 
   Given Launch the browser and Load the DP Url   
   And Enter Credentials and login1
   And Mouseover on Salesforce Tab
   And Click Beat
   Then Click Add Beat button
   And Enter required Beat details from dataSheet1
   And Click BeatSave
   Then Click Search and confirm Beat creation1
   
@smoke   
Scenario: Login to DP and Create Salesman
   Given Launch the browser and Load the DP Url   
   And Enter Credentials and login1
   And Mouseover on Salesforce Tab
   And Click Salesman
   Then Click Add Salesman button
   And Enter required Salesman details from dataSheet1
   #And Click SalesmanSave
   Then Click Search and confirm Salesman creation1
   #And Click created Salesman
   #Then Add Beat to the Salesman
      
   
 Scenario: Login to DP and Create Customer
   Given Launch the browser and Load the DP Url   
   And Enter Credentials and login1
   And Mouseover on Masters Tab
   And Click Customer
   Then Click Add Customer button
   And Enter required Customer details from dataSheet1
   And Click CustomerSave
   Then Click Search and confirm Customer creation1
   And Click created Customer
   Then Add Beat to the Customer
   
Scenario: Login to DP and Create MJP
   Given Launch the browser and Load the DP Url   
   And Enter Credentials and login1
   And Mouseover on Salesforce Tab
   And Click MJP
   Then Click Add MJP button
   And Enter required MJP details from dataSheet1
   And Click MJPSave
   Then Click Search and confirm MJP creation1  
   
Scenario: Login to DP and Create Sales Order
   Given Launch the browser and Load the DP Url   
   And Enter Credentials and login1
   And Mouseover on SalesManagement Tab
   And Click Sales Order
   Then Click Add Sales Invoice button
   And Enter required Invoice details from dataSheet1
   And Verify Free items and Scheme quantity
   Then Calculate all the prices
   And Click SOSave
   
   
   
   
   
   
     