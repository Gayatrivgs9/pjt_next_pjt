package pages.forum.next;

import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.NoSuchElementException;

import com.cucumber.listener.Reporter;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import lib.selenium.WebDriverServiceImpl;

public class GSKsmoke extends WebDriverServiceImpl{


	@Given("^Clear the Beat Excel Values(\\d+)$")
	public void clearExcelBeat(int arg1) throws Throwable {
		writeData(6, arg1, "", "Beat"); // BeatName
		writeData(1, arg1, "", "Beat"); // BeatCode
		writeData(7, arg1, "", "Beat"); // Status
	}

	@And("^Mouseover on Salesforce Tab$")
	public void mouseoverOnSalesforceTab() throws Throwable {	    
		MouseOver(locateElement("link", "SalesForce_lt"));	   
	}

	@And("^Mouseover on SalesManagement Tab$")
	public void mouseoverOnSalesManagementTab() throws Throwable {
		MouseOver(locateElement("link", "SalesManagement_lt"));
	}

	@And("^Mouseover on Masters Tab$")
	public void mouseoverOnMastersTab() throws Throwable {
		MouseOver(locateElement("link", "Masters_lt"));
	}

	@And("^Click Beat$")
	public void clickBeat() throws Throwable {
		click(locateElement("xpath", "Beat_xp"));
	}

	@And("^Click Salesman$")
	public void clickSalesman() throws Throwable {
		click(locateElement("xpath", "Salesman_xp"));
	}

	@And("^Click Customer$")
	public void clickCustomer() throws Throwable {
		click(locateElement("xpath", "Customer_xp"));
	}

	@And("^Click MJP$")
	public void clickMJP() throws Throwable {
		click(locateElement("xpath", "MJP_xp"));
	}

	@And("^Click Sales Order$")
	public void clickSalesOrder() throws Throwable {
		click(locateElement("xpath", "SO_xp"));
	}
	@And("^Click Bulk Order Conversion$")
	public void clickBulkOrderConversion() throws Throwable {
		click(locateElement("xpath", "BulkOrderConversion_xp"));
	}
	@And("^Click Sales Invoice$")
	public void clickSalesInvoice() throws Throwable {
		click(locateElement("xpath", "SI_xp"));
	}

	@Then("^Click Add Beat button$")
	public void clickAddBeatButton() throws Throwable {
		click(locateElement("xpath", "CreateBeat_xp"));
	}

	@Then("^Click Add Salesman button$")
	public void clickAddSalesmanButton() throws Throwable {
		click(locateElement("xpath", "CreateSalesMan_xp"));
	}

	@Then("^Click Add Customer button$")
	public void clickAddCustomerButton() throws Throwable {
		click(locateElement("xpath", "CreateCustomer_xp"));
	}

	@Then("^Click Add MJP button$")
	public void clickAddMJPButton() throws Throwable {
		click(locateElement("xpath", "CreateMJP_xp"));
	}

	@Then("^Click Add Sales Invoice button$")
	public void clickAddSalesInvoiceButton() throws Throwable {
		click(locateElement("xpath", "CreateSI_xp"));
	}

	@Then("^Click Add Sales order button$")
	public void clickAddSalesOrderButton() throws Throwable {
		click(locateElement("xpath", "CreateSO_xp"));
	}


	public String beatName;

	@And("^Enter required Beat details from dataSheet(\\d+)$")
	public void enterRequiredBeatDetailsFromDataSheet(int arg1) throws Throwable {

		// BeatName		
		beatName = readDataByColumnName("Beat", "BeatName", arg1); 
		type(locateElement("id", "BeatName_id"), beatName);
		writeData(6, arg1, beatName, "Beat");


		// BeatCode
		String beatCode = locateElement("id", "BeatCode_id").getAttribute("value"); 
		writeData(1, arg1, beatCode, "Beat");


		// BeatActive
		String ActiveBeat = readDataByColumnName("Beat", "Active", arg1);

		if(ActiveBeat.equals("Yes")||ActiveBeat.equals("Y")||ActiveBeat.equals("YES")){
			VerifyBeforeSelect(locateElement("name", "Active_nm"));
		}
		else if(ActiveBeat.equals("No")||ActiveBeat.equals("N")||ActiveBeat.equals("NO")){
			VerifyBeforeUnSelect(locateElement("name", "Active_nm"));
		}


		// Distance in KM
		String DistanceinKM = readDataByColumnName("Beat", "DistanceInKM", arg1); 
		type(locateElement("id", "Distance_id"), DistanceinKM);

		// Classification
		String Classification = readDataByColumnName("Beat", "Classification", arg1); 
		selectDropDownUsingVisibleText(locateElement("name", "Classification_nm"), Classification);


		//Counter Sales Beat
		String CounterSalesBeat = readDataByColumnName("Beat", "CountersalesBeat", arg1);

		if(CounterSalesBeat.equals("Yes")||CounterSalesBeat.equals("Y")||CounterSalesBeat.equals("YES")){
			VerifyBeforeSelect(locateElement("name", "CounterSalesBeat_nm"));
		}
		else if(CounterSalesBeat.equals("No")||CounterSalesBeat.equals("N")||CounterSalesBeat.equals("NO")){
			VerifyBeforeUnSelect(locateElement("name", "CounterSalesBeat_nm"));
		}		


		Reporter.addStepLog("Beat Details entered in respective field");
		Reporter.addScreenCaptureFromPath(captureScreenShot("BeatDetailsEntered"));	

	}


	public String salesmanName;

	@And("^Enter required Salesman details from dataSheet(\\d+)$")
	public void enterRequiredSalesmanDetailsFromDataSheet(int arg1) throws Throwable {

		// salesmanName		
		salesmanName = readData(0, arg1, "Salesman"); 
		type(locateElement("id", "Salesman_id"), salesmanName);
		writeData(1, arg1, salesmanName, "Salesman");

		// salesmanCode
		String salesmanCode = locateElement("id", "SalesmanCode_id").getAttribute("value"); 
		writeData(2, arg1, salesmanCode, "Salesman");

		// SalesmanCategoryGroup
		String SalesmanCategoryGroup = readDataByColumnName("Salesman", "SalesmanCategoryGroup", arg1);
		inputComboTextidPagination(prop.getProperty("SalesmanCategoryGroup_id"), SalesmanCategoryGroup);
		Thread.sleep(10000);
	}	

	public String customerName;
	@And("^Enter required Customer details from dataSheet(\\d+)$")
	public void enterRequiredCustomerDetailsFromDataSheet(int arg1) throws Throwable {

	}
	public String MJPName;
	@And("^Enter required MJP details from dataSheet(\\d+)$")
	public void enterRequiredMJPDetailsFromDataSheet(int arg1) throws Throwable {

	}

	@And("^Enter required Invoice details from dataSheet(\\d+)$")
	public void enterRequiredInvoiceDetailsFromDataSheet(int arg1) throws Throwable {

	}
	public String salesOrder; 
	@And("^Enter required Sales Order details from dataSheet(\\d+)$")
	public void enterRequiredSalesOrderDetailsFromDataSheet(int arg1) throws Throwable {
		salesOrder = readDataByColumnName("SalesOrder", "SalesMan", arg1);
		System.out.println(salesOrder); 
		click(locateElement("xpath", "SO_SalesManName_xpath"));
		type(locateElement("xpath", "SO_SalesManName_inputbox_xpath"), salesOrder);
		Thread.sleep(2000); 
		click(locateElement("xpath", "SO_SalesManName_table_xpath"));
		click(locateElement("xpath", "SO_CustomerName_xpath"));
		click(locateElement("xpath", "SO_CustomerName_tablexpath"));
		type(locateElement("xpath",  "SO_ReferenceDescription_xpath"), readDataByColumnName("SalesOrder", "Reference Description", arg1)); 
		type(locateElement("id", "SO_tracking_Reference_id"), readDataByColumnName("SalesOrder", "Reference Number", arg1));
		click(locateElement("xpath", "SO_CreditTerm_xpath"));
		click(locateElement("xpath", "SO_CreditTerm_table_xpath"));
		click(locateElement("xpath", "SO_Beat_xpath"));
		click(locateElement("xpath", "SO_Beat_table_xpath"));
		click(locateElement("xpath", "SO_TransactionSeries_xpath"));
		click(locateElement("xpath", "SO_TransactionSeries_table_xpath"));
		click(locateElement("xpath", "SO_ShippingAddressPick_xpath"));
		click(locateElement("xpath", "SO_ShippingAddressPick_table_xpath"));
		click(locateElement("xpath", "SO_BillingAddressPick_xpath"));
		click(locateElement("xpath", "SO_BillingAddressPick_table_xpath"));
	}
	public static String productPrice;
	public static String productTotal;
	public static String discountTotal;
	public static String totalAfterDiscount;
	public static String taxTotal;
	public static String schemeDiscDisplay;
	public static String netRate;
	public static String netPrice;
	@And("^Validate the product tatal price from dataSheet(\\d+)$")
	public void verifyProductTotal(int arg1) throws IOException, EncryptedDocumentException, InvalidFormatException { 
		type(locateElement("id", "productName1_id"),  readDataByColumnName("SalesOrder", "ProductName", arg1));
		type(locateElement("id", "qty1_id"),  readDataByColumnName("SalesOrder", "Qty", arg1));
		selectDropDownUsingIndex(locateElement("id", "tUOM1_id"), 0);
		productPrice = getAttribute(locateElement("id", "listPrice1_id"), "value");
		writeData(6, arg1, productPrice, "SalesOrder"); 
		productTotal = getText(locateElement("id", "productTotal1_id"));
		writeData(7, arg1, productTotal, "SalesOrder");
		discountTotal = getText(locateElement("id", "discountTotal1_id"));
		writeData(8, arg1, discountTotal, "SalesOrder");
		totalAfterDiscount = getText(locateElement("id", "totalAfterDiscount1_id"));
		writeData(9, arg1, totalAfterDiscount, "SalesOrder");
		taxTotal = getText(locateElement("id", "taxTotal1_id"));
		writeData(10, arg1, taxTotal, "SalesOrder");
		schemeDiscDisplay = getText(locateElement("id", "schemeDiscDisplay1_id"));
		writeData(11, arg1, schemeDiscDisplay, "SalesOrder");
		netRate = getText(locateElement("id", "netRate1_id"));
		writeData(12, arg1, netRate, "SalesOrder");
		netPrice = getText(locateElement("id", "netPrice1_id"));
		writeData(13, arg1, netPrice, "SalesOrder");
	}

	@And("^Click BeatSave$")
	public void clickBeatSave() throws Throwable {

		click(locateElement("xpath", "CreationSave_xp"));				

		acceptAlert();

		Reporter.addStepLog("Beat Save Clicked");
		Reporter.addScreenCaptureFromPath(captureScreenShot("BeatSaveClicked"));	

	}

	@And("^Click SalesmanSave$")
	public void clickSalesmanSave() throws Throwable {

		click(locateElement("xpath", "CreationSave_xp"));				

		acceptAlert();

		Reporter.addStepLog("Salesman Save Clicked");
		Reporter.addScreenCaptureFromPath(captureScreenShot("SalesmanSaveClicked"));

	}

	@And("^Click CustomerSave$")
	public void clickCustomerSave() throws Throwable {

		click(locateElement("xpath", "CreationSave_xp"));

		acceptAlert();

		Reporter.addStepLog("Customer Save Clicked");
		Reporter.addScreenCaptureFromPath(captureScreenShot("CustomerSaveClicked"));

	}

	@And("^Click MJPSave$")
	public void clickMJPSave() throws Throwable {

		click(locateElement("xpath", "CreationSave_xp"));

		acceptAlert();

		Reporter.addStepLog("MJP Save Clicked");
		Reporter.addScreenCaptureFromPath(captureScreenShot("MJPSaveClicked"));

	}

	@Then("^Click SOSave$")
	public void clickSOSave() throws Throwable {

		click(locateElement("xpath", "CreationSave_xp"));

		acceptAlert();

		Reporter.addStepLog("SO Save Clicked");
		Reporter.addScreenCaptureFromPath(captureScreenShot("SOSaveClicked"));

	}

	@Then("^Click SISave$")
	public void clickSISave() throws Throwable {

		click(locateElement("xpath", "CreationSave_xp"));

		acceptAlert();

		Reporter.addStepLog("SI Save Clicked");
		Reporter.addScreenCaptureFromPath(captureScreenShot("SISaveClicked"));

	}

	@Then("^Click Search and confirm Beat creation(\\d+)$")
	public void clickSearchAndConfirmBeatCreation(int arg1) throws Throwable {

		click(locateElement("xpath", "BeatSearch_xp"));

		type(locateElement("xpath", "SearchBox_xp"), beatName);

		click(locateElement("xpath", "SearchNow_xp"));

		try {			
			driver.findElementByXPath("//a[text()='"+beatName+"']");

			Reporter.addStepLog("Created Beat is available in Grid");
			Reporter.addScreenCaptureFromPath(captureScreenShot("BeatSaved"));	
			writeData(7, arg1, "PASS", "Beat");
			assertEquals(true, true);

		} catch (NoSuchElementException e) {
			Reporter.addStepLog("Beat is not Created");
			Reporter.addScreenCaptureFromPath(captureScreenShot("ErrorInBeatCreation"));
			writeData(7, arg1, "FAIL", "Beat");
			assertEquals(false, true);
		}		
	}


	@Then("^Click Search and confirm Salesman creation(\\d+)$")
	public void clickSearchAndConfirmSalesmanCreation(int arg1) throws Throwable {

		click(locateElement("xpath", "SalesmanSearch_xp"));
		type(locateElement("xpath", "SearchBox_xp"), salesmanName);
		click(locateElement("xpath", "SearchNow_xp"));

		try {			
			driver.findElementByXPath("//a[text()='"+salesmanName+"']");

			Reporter.addStepLog("Created Salesman in available in Grid");
			Reporter.addScreenCaptureFromPath(captureScreenShot("SalesmanCreated"));	
			writeData(7, arg1, "PASS", "Salesman");
			assertEquals(true, true);

		} catch (NoSuchElementException e) {
			Reporter.addStepLog("Error in Salesman Creation");
			Reporter.addScreenCaptureFromPath(captureScreenShot("ErrorInSalesmanCreation"));
			writeData(7, arg1, "FAIL", "Salesman");
			assertEquals(false, true);
		}		

	}

	@And("^Click Search and confirm Customer creation(\\d+)$")
	public void clickSearchAndConfirmCustomerCreation(int arg1) throws Throwable {

		click(locateElement("xpath", "CustomerSearch_xp"));

		type(locateElement("xpath", "SearchBox_xp"), customerName);

		click(locateElement("xpath", "SearchNow_xp"));

		try {			
			driver.findElementByXPath("//a[text()='"+customerName+"']");

			Reporter.addStepLog("Created Customer in available in Grid");
			Reporter.addScreenCaptureFromPath(captureScreenShot("CustomerCreated"));	
			writeData(7, arg1, "PASS", "Customer");
			assertEquals(true, true);

		} catch (NoSuchElementException e) {
			Reporter.addStepLog("Error in Customer Creation");
			Reporter.addScreenCaptureFromPath(captureScreenShot("ErrorInCustomerCreation"));
			writeData(7, arg1, "FAIL", "Customer");
			assertEquals(false, true);
		}		

	}


	@Then("^Click Search and confirm MJP creation(\\d+)$")
	public void clickSearchAndConfirmMJPCreation(int arg1) throws Throwable {

		click(locateElement("xpath", "MJPSearch_xp"));

		type(locateElement("xpath", "SearchBox_xp"), MJPName);

		click(locateElement("xpath", "SearchNow_xp"));

		try {			
			driver.findElementByXPath("//a[text()='"+MJPName+"']");

			Reporter.addStepLog("Created MJP is available in Grid");
			Reporter.addScreenCaptureFromPath(captureScreenShot("MJPCreated"));	
			writeData(7, arg1, "PASS", "MJP");
			assertEquals(true, true);

		} catch (NoSuchElementException e) {
			Reporter.addStepLog("Error in MJP Creation");
			Reporter.addScreenCaptureFromPath(captureScreenShot("ErrorInMJPCreation"));
			writeData(7, arg1, "FAIL", "MJP");
			assertEquals(false, true);
		}	

	}

	@And("^Click created Customer$")
	public void clickCreatedCustomer() throws Throwable {
		driver.findElementByXPath("//a[text()='"+customerName+"']").click();
		Reporter.addStepLog("Created Clicked");
		Reporter.addScreenCaptureFromPath(captureScreenShot("Customerpage"));			

	}	


	@And("^Click created Salesman$")
	public void clickCreatedSalesman() throws Throwable {
		driver.findElementByXPath("//a[text()='"+salesmanName+"']").click();
		Reporter.addStepLog("Salesman Clicked");
		Reporter.addScreenCaptureFromPath(captureScreenShot("Salesmanpage"));	

	}

	@Then("^Add Beat to the Salesman$")
	public void addBeatToTheSalesman() throws Throwable {

	}


	@Then("^Add Beat to the Customer$")
	public void addBeatToTheCustomer() throws Throwable {

	}


	@And("^Verify Free items and Scheme quantity$")
	public void verifyFreeItemsAndSchemeQuantity() throws Throwable {

	}

	@And("^Calculate all the prices$")
	public void calculateAllThePrices() throws Throwable {

	}





















}
