package runner;

import java.io.File;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import com.cucumber.listener.ExtentProperties;
import com.cucumber.listener.Reporter;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;


@CucumberOptions(features = "features", plugin = {"pretty","com.cucumber.listener.ExtentCucumberFormatter:output/report.html"},glue = { "Selenium.Cucumber.Livewire" })
public class RunnerTestNG extends AbstractTestNGCucumberTests {
	
	static String path = System.getProperty("user.dir");
	@AfterClass
    public static void setup() {
        Reporter.loadXMLConfig(new File(path+ "/src/test/java/propertiesFile/extent-config.xml"));
        Reporter.setSystemInfo("user", System.getProperty("user.name"));
        Reporter.setSystemInfo("OS", "Windows 10");
        Reporter.setTestRunnerOutput("Sample test runner output message");
    }
	
	@BeforeClass
    public static void extendReport() {
        ExtentProperties extentProperties = ExtentProperties.INSTANCE;
//        extentProperties.setExtentXServerUrl("http://localhost:1337");
//        extentProperties.setProjectName("MyProject");
        extentProperties.setReportPath(path + "//src//test//java//reports//TestNG_ExecutionResult"+ System.currentTimeMillis()+".html");
    }
}