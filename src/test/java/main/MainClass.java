package main;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import cucumber.api.cli.Main;


public class MainClass {
	
	public static boolean newNotePad = false;
	public static int scenarioCount=1;
	public static FileWriter fw = null;
	public static BufferedWriter bw = null;
	public static String[] scenarioName = new String[500];
	public static String[] scenarioStatus = new String[500];
	public static int passCount = 0;
	public static int failCount = 0;
	public static String report_Date = null;

	public static void main(String[] args) throws Throwable
	{	
		SimpleDateFormat dt = new SimpleDateFormat("yyyy_MM_dd");
		Date date = new Date();
		String date_s = dt.format(date);
		report_Date = date_s;
		
		Main.main(new String[]{								

				"--glue",  "pages.forum.next",
				    "-p",  "com.cucumber.listener.ExtentCucumberFormatter:output/report_"+report_Date+".html",
			      //"-t",  "@smoke",
				
				"classes//features//LoginForumNext.feature",
				"classes//features//AutoMail.feature",
				}				

           );
	}
}












