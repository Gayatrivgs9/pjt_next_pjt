package pages.forum.next;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.cucumber.listener.Reporter;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import lib.selenium.PreAndPost;

public class Hooks extends PreAndPost{
	
	public static RemoteWebDriver driver;
	public static String fileExtn = "";
	@Before
	public void Initializetest() throws IOException {
		try {
			if(environment.equals("local")) {
				System.setProperty("webdriver.chrome.driver", "./src/test/java/drivers/chromedriver.exe");
				driver = new ChromeDriver();
				driver.manage().window().maximize();
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			}else {
				FirefoxBinary firefoxBinary = new FirefoxBinary();
				firefoxBinary.addCommandLineOptions("--headless");
				System.setProperty("webdriver.gecko.driver", "/usr/local/bin/geckodriver");
				FirefoxOptions firefoxOptions = new FirefoxOptions();             
				firefoxOptions.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, false);             
				firefoxOptions.setBinary(firefoxBinary);
				driver = new FirefoxDriver(firefoxOptions);          
				driver.manage().window().maximize();
				driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				System.out.println("Test Started");
				//Reporter.addStepLog("The browser: "+browser+" launched successfully");
			}} catch (WebDriverException e) {		 	
				Reporter.addStepLog("The browser: chrome could not be launched");
				Reporter.addScreenCaptureFromPath(captureScreenShot("BrowserNotLaunched"));
			}
	}
	
	@After
	public void teardowntest(Scenario scenario) throws Exception {
		if (scenario.isFailed()){
			System.err.println("*********************************************");
			System.out.println("Scenario status :: " + scenario.getStatus());
			System.out.println("Scenario name :: " + scenario.getName());
			System.out.println("Test Ended with fail status");
			System.err.println("*********************************************");
			String path = System.getProperty("user.dir");
			String name = scenario.getName();
			SimpleDateFormat dt = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
			Date date = new Date();
			String date_s = dt.format(date);
			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(screenshot,
					new File(path + "//src//test//java//snaps//FailedScenarios//" + name + "_" + date_s + ".png"));
			Reporter.addScreenCaptureFromPath(path + "//src//test//java//snaps//FailedScenarios//" + name + "_" + date_s + ".png", "FailedScenario_"+scenario.getName());
			closeAllBrowsers();
		} else {
			System.err.println("*********************************************");
			System.out.println("Scenario status :: " + scenario.getStatus());
			System.out.println("Scenario name :: " + scenario.getName());
			System.out.println("Test Ended");
			System.err.println("*********************************************");
			closeAllBrowsers();
		}
	}
	
}
