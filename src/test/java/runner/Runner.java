package runner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import com.cucumber.listener.ExtentProperties;
import com.cucumber.listener.Reporter;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = {
                              "src/test/java/features/SingleBulkConversion.feature"},
		                    //"src/test/java/features/LoginForumNext.feature",
							// "src/test/java/features/GSKsmoke.feature"},
				//	 tags = {"@smoke1"},
                     glue = {"pages.forum.next"},
               monochrome = true,
                   dryRun = false,
                   strict = true,
                 snippets = SnippetType.CAMELCASE,
                   plugin = { "pretty", "com.cucumber.listener.ExtentCucumberFormatter:","rerun:target/rerun.txt"}) 


public class Runner {
	static String path = System.getProperty("user.dir");

	@AfterClass
	public static void setup() {
		Reporter.loadXMLConfig(new File(path + "/src/test/java/propertiesFile/extent-config.xml"));
		Reporter.setSystemInfo("user", System.getProperty("user.name"));
		Reporter.setSystemInfo("OS", "Windows 10");
		Reporter.setTestRunnerOutput("Sample test runner output message");
	}

	@BeforeClass
	public static void extendReport() throws IOException {
		ExtentProperties extentProperties = ExtentProperties.INSTANCE;
		SimpleDateFormat dt = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
		Date date = new Date();
		String date_s = dt.format(date);
		extentProperties.setProjectName("ForumNextCucumber");
		File f1 = new File(System.getProperty("user.dir") + "\\src\\test\\java\\reports");
		if (f1.exists()) {
			//System.err.println("folder available");
		} else {
			f1.mkdirs();
			//System.out.println("Folder Created");
		}
		extentProperties.setReportPath("./src/test/java/reports/Junit_ExecutionResult" + date_s + ".html");
		backupData();
	}

	public static void backupData() throws IOException {

		SimpleDateFormat dt = new SimpleDateFormat("yyyy_MM_dd_hh_mm_ss");
		Date date = new Date();
		String date_s = dt.format(date);

		Workbook wb1 = null;
		String path = System.getProperty("user.dir");
		String fileName = path + "\\src\\test\\java\\data\\DataSheet.xlsx";
		FileInputStream fileInputStream = new FileInputStream(fileName);
		String fileExtensionName = fileName.substring(fileName.indexOf("."));
		if (fileExtensionName.equals(".xlsx")) {
			wb1 = new XSSFWorkbook(fileInputStream);
			XSSFFormulaEvaluator.evaluateAllFormulaCells(wb1);
		}

		else if (fileExtensionName.equals(".xls")) {
			wb1 = new HSSFWorkbook(fileInputStream);
			HSSFFormulaEvaluator.evaluateAllFormulaCells(wb1);
		}

		FileOutputStream fileOut = new FileOutputStream(
				path + "//src//test//java//data//DataSheetBackup//DataSheet" + date_s + ".xlsx");
		wb1.write(fileOut);
		fileOut.close();

	}


}